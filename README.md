# Installation

* Install the module as every other Drupal module
* Download the atjs javascript library to the ```/libraries``` folder
  so it looks like the following :
```
libraries
  - at
  --- ...
  --- dict
```

# Usage

* Add a "Entity references with text" field
* Leverage it in the output
