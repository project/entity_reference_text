<?php

namespace Drupal\entity_reference_text\Element;

use Drupal\Core\Entity\Element\EntityAutocomplete;

/**
 * @FormElement("entity_autocomplete_text")
 */
class EntityAutocompleteWithText extends EntityAutocomplete {

}
